package com.shoppingkart.automation.test.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.annotations.Test;

import com.shoppingkart.utils.PropertyReader;

public class ProductAPITest2 {
	HttpClient httpClient = HttpClientBuilder.create().build();
	PropertyReader apiProperties = new PropertyReader("product-api.properties");

	@Test
	public void createProduttest() throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost("http://mishoppingkart.com/ShoppingCart/api/addProduct");
		System.out.println(
				"Create the product with the data --- " + apiProperties.getProperty("createProductJsonString"));
		StringEntity albumPayload = new StringEntity(apiProperties.getProperty("productPayloadJsonString"));
		albumPayload.setContentType("application/json");
		httpPost.setEntity(albumPayload);
		HttpResponse response = httpClient.execute(httpPost);
		System.out.println(response.getStatusLine().getStatusCode());
		String line = null;
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}

	@Test
	public void updateProdutTest() throws ClientProtocolException, IOException {
		HttpPut httpPut = new HttpPut("http://mishoppingkart.com/ShoppingCart/api/editProduct");
		System.out
				.println("Update the product with the data ---" + apiProperties.getProperty("updateProductJsonString"));
		StringEntity albumPayload = new StringEntity(apiProperties.getProperty("updateProductJsonString"));
		albumPayload.setContentType("application/json");
		httpPut.setEntity(albumPayload);
		HttpResponse response = httpClient.execute(httpPut);
		System.out.println(response.getStatusLine().getStatusCode());
		String line = null;
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}

	@Test
	public void getProductTest() throws Exception {

		HttpGet request = new HttpGet("http://mishoppingkart.com/ShoppingCart/api/viewProduct/106");

		request.addHeader("accept", "application/json");

		HttpResponse response = httpClient.execute(request);

		System.out.println(" Get request with status code " + response.getStatusLine().getStatusCode());

		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

		String output = null;
		System.out.println("Output from Server .... \n");
		System.out.println("outpouutt from GET request:-----");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		System.out.println("outpouutt from GET ends hereeee :-----");

	}

	@Test
	public void deleteProductTest() throws Exception {

		HttpDelete request = new HttpDelete("http://mishoppingkart.com/ShoppingCart/api/deleteProduct/106");

		request.addHeader("accept", "application/json");

		HttpResponse response = httpClient.execute(request);

		System.out.println("VIN Delete request with status code " + response.getStatusLine().getStatusCode());

		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

		String output = null;
		System.out.println("Output from Server .... \n");
		System.out.println("outpouutt from DELETE request:-----");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
		System.out.println("outpouutt from DELETE ends hereeee :-----");

	}

	public int getRandomNumber() {
		Random random = new Random();
		int randomNum = random.nextInt(100000);
		System.out.println(randomNum);
		return randomNum;

	}
}