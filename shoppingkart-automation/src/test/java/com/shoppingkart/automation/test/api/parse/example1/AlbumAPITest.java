package com.shoppingkart.automation.test.api.parse.example1;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.shoppingkart.automation.test.api.Album;
import com.shoppingkart.utils.PropertyReader;

public class AlbumAPITest {

	HttpClient httpClient = HttpClientBuilder.create().build();
	PropertyReader albumProperties = new PropertyReader("album-api-data.properties");

	@Test
	public void getRequestTest() throws Exception {

		HttpPost request = new HttpPost("http://localhost:3000/albums");

		request.addHeader("accept", "application/json");

		HttpResponse response = httpClient.execute(request);

		System.out.println("VIN Get request with status code " + response.getStatusLine().getStatusCode());
		Album album = parseTheResponse(response);
		System.out.println(album.getId());
		System.out.println(album.getTitle());
		System.out.println(album.getUserId());

	}

	@Test
	public void createAlbumTest() throws Exception {
		HttpPost httpPost = new HttpPost("http://localhost:3000/albums");
		System.out
				.println("Update the product with the data ---" + albumProperties.getProperty("creatAlbumJsonString"));
		StringEntity albumPayload = new StringEntity(albumProperties.getProperty("creatAlbumJsonString"));
		albumPayload.setContentType("application/json");
		httpPost.setEntity(albumPayload);
		HttpResponse response = httpClient.execute(httpPost);
		System.out.println("VIN Get request with status code " + response.getStatusLine().getStatusCode());
		String line = null;
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
		Album album = parseTheResponse(response);
		
		System.out.println(album.getId());
		System.out.println(album.getTitle());
		System.out.println(album.getUserId());
	}
	// Method for parsing Product response

	public Album parseTheResponse(HttpResponse response) throws Exception {
		// Read and store the response json
		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

		String albumJson = null;
		// Create object for Product ... see Product.java for details

		Album albumObject = null;
		// Iterate through the response and parse it

		while ((albumJson = br.readLine()) != null) {
			System.out.println("Product json response :" + albumJson);
			try {
				// JsonReader will read and store the data

				JsonReader reader = new JsonReader(new StringReader(albumJson));
				reader.setLenient(true);
				// create Gson object
				Gson gson = new GsonBuilder().setLenient().create();
				// Gson will load ur Json reader into a java object, in this
				// case Product object
				albumObject = gson.fromJson(reader, Album.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return albumObject;

	}

	public int getRandomNumber() {
		Random random = new Random();
		int randomNum = random.nextInt(100000);
		System.out.println(randomNum);
		return randomNum;

	}
}
