package com.shoppingkart.automation.test.api.parsingexample;

import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class ParseTheProductDataTest {

	HttpClient httpClient = HttpClientBuilder.create().build();
	String key = null;

	@Test
	public void getRequestTest() throws Exception {

		HttpGet request = new HttpGet("http://mishoppingkart.com/ShoppingCart/api/viewProduct/36");

		request.addHeader("accept", "application/json");

		HttpResponse response = httpClient.execute(request);

		System.out.println("VIN Get request with status code " + response.getStatusLine().getStatusCode());
		Product productdata = parseTheProductResponse(response);
		System.out.println(productdata.getId());
		System.out.println(productdata.getName());
		System.out.println(productdata.getCategory());
		assertTrue(productdata.getId() == 32, " Id is not matching ");
		assertTrue(productdata.getName().equals("Veena"), " Name is not matching ");

	}

	// Method for parsing Product response

	public Product parseTheProductResponse(HttpResponse response) throws Exception {
		// Read and store the response json
		BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

		String productJson = null;
		// Create object for Product ... see Product.java for details

		Product productObject = null;
		// Iterate through the response and parse it

		while ((productJson = br.readLine()) != null) {
			System.out.println("Product json response :" + productJson);
			try {
				// JsonReader will read and store the data

				JsonReader reader = new JsonReader(new StringReader(productJson));
				reader.setLenient(true);
				// create Gson object

				Gson gson = new GsonBuilder().setLenient().create();
				// Gson will load ur Json reader into a java object, in this
				// case Product object
				productObject = gson.fromJson(reader, Product.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return productObject;

	}

}
