package com.shoppingkart.test.product;

import static org.testng.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.shoppingkart.pages.home.ShoppingKartHomePage;
import com.shoppingkart.pages.product.ShoppingKartProductPage;
import com.shoppingkart.test.base.ShoppingKartBaseTest;
import com.shoppingkart.utils.DBReader;

public class ProductSearchTest extends ShoppingKartBaseTest {

	public static final Logger logger = LogManager.getLogger(ShoppingKartProductTest.class);
	ShoppingKartHomePage homePage;
	ShoppingKartProductPage productPage;

	@Test
	public void isHomeLoadedTest() {
		homePage = new ShoppingKartHomePage();
		assertTrue(homePage.isHomePageLoaded(), "google home page logo is not loaded ");
		logger.info(" verified the homepage");
	}

	@Test(dependsOnMethods = "isHomeLoadedTest")
	public void isProductPageLoadedTest() {
		productPage = homePage.clickOnProducts();
		assertTrue(productPage.isProductPageLoaded(), "Product page is not loaded");
	}

	// Type 1 test create query and get the data
	@Test(dependsOnMethods = "isProductPageLoadedTest")
	public void searchProductTest() throws SQLException {

		ResultSet rs = DBReader.runQuery("select * from product");

		while (rs.next()) {
			logger.info("product from database is - " + rs.getString("name"));
			productPage.searchForProduct(rs.getString("name"));
			break;
		}

	}

	// Type 2 use the readymade method to get product
	@Test(dependsOnMethods = "isProductPageLoadedTest")
	public void searchProductTest2() throws SQLException {

		String product = DBReader.getFirstProduct();

		productPage.searchForProduct(product);

	}

}
