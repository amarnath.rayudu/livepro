package com.shoppingkart.test.product;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.shoppingkart.pages.home.ShoppingKartHomePage;
import com.shoppingkart.pages.product.ShoppingKartProductPage;
import com.shoppingkart.test.base.ShoppingKartBaseTest;

public class ShoppingKartProductTest extends ShoppingKartBaseTest {
	public static final Logger logger = LogManager.getLogger(ShoppingKartProductTest.class);
	ShoppingKartHomePage homePage;
	ShoppingKartProductPage productPage;
	

	@Test
	public void isHomeLoadedTest() {
		homePage = new ShoppingKartHomePage();
		assertTrue(homePage.isHomePageLoaded(), "google home page logo is not loaded ");
		logger.info(" verified the homepage");
	}
	@Test(dependsOnMethods="isHomeLoadedTest")
	public void isProductPageLoadedTest() {
		productPage = homePage.clickOnProducts();
		logger.info("Clicked on product page");
		assertTrue(productPage.categoryPresent(), "categorytab is not present");
		logger.info(" verified the categoryPresent");
		assertTrue(productPage.conditionPresent(), "condition is not present");
		logger.info(" verified the conditionPresent");
		assertTrue(productPage.pricePresent(), "price is not present");
		logger.info(" verified the pricePresent");
		assertTrue(productPage.productNamePresent(), "productName is not present");
		logger.info(" verified the productNamePresent");
		assertTrue(productPage.thumbnailPresent(), "thumbnail is not present");
		logger.info(" verified the thumbnailPresent");

	}


}
