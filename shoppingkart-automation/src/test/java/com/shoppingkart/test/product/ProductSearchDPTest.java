package com.shoppingkart.test.product;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.shoppingkart.pages.home.ShoppingKartHomePage;
import com.shoppingkart.pages.product.ShoppingKartProductPage;
import com.shoppingkart.test.base.ShoppingKartBaseTest;
import com.shoppingkart.utils.ExcelReader;

public class ProductSearchDPTest extends ShoppingKartBaseTest {

	public static final Logger logger = LogManager.getLogger(ShoppingKartProductTest.class);
	ShoppingKartHomePage homePage;
	ShoppingKartProductPage productPage;

	@Test
	public void isHomeLoadedTest() {
		homePage = new ShoppingKartHomePage();
		assertTrue(homePage.isHomePageLoaded(), "google home page logo is not loaded ");
		logger.info(" verified the homepage");
	}

	@Test(dependsOnMethods = "isHomeLoadedTest")
	public void isProductPageLoadedTest() {
		productPage = homePage.clickOnProducts();
		assertTrue(productPage.isProductPageLoaded(), "Product page is not loaded");
	}

	// use data provider to get the static tests
	@Test(dependsOnMethods = "isProductPageLoadedTest", dataProvider = "searchData")
	public void searchProductTest(String searchProduct) {
		productPage.searchForProduct(searchProduct);
		assertTrue(productPage.areResultsLoaded(), "Resluts not loaded ");
	}

	/**
	 * @throws Exception
	 */
	@DataProvider(name = "searchData")
	public Object[][] searchData() throws Exception {
		String sheetName = "";
		String filePath = "src/test/resources/testdata/ProductSearchData.xlsx";
		logger.info("Test data is loaded from file " + filePath + " and the sheet is " + sheetName);
		Object[][] testObjArray = ExcelReader.getTableArray(filePath, "searchTest");

		return (testObjArray);

	}

}
