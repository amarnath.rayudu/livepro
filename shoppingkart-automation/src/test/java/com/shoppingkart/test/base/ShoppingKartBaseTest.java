package com.shoppingkart.test.base;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.shoppingkart.pages.base.ShoppingKartBasePage;
import com.shoppingkart.utils.PropertyReader;

//@Listeners({ com.shoppingkart.automation.listeners.ShoppingKartListener.class })
public class ShoppingKartBaseTest {
	public static WebDriver driver;
	public static final Logger logger = LogManager.getLogger(ShoppingKartBaseTest.class);

	@BeforeSuite
	public void init() {
		PropertyReader propertyReader = new PropertyReader("env.properties");
		String browser = propertyReader.getProperty("browser");

		if (browser.equals("firefox")) {
			String firefoxPath = propertyReader.getProperty("localFirefoxPath");
			logger.info("geckodriver location is " + firefoxPath);
			System.setProperty("webdriver.gecko.driver", firefoxPath);
			driver = new FirefoxDriver();
		} else if (browser.equals("chrome")) {
			String chromePath = propertyReader.getProperty("localChromePath");
			logger.info("chromedriver location is " + chromePath);
			System.setProperty("webdriver.chrome.driver", chromePath);
			logger.info("chromedriver location is " + chromePath);
			driver = new ChromeDriver();
		} else if (browser.equals("IE")) {
			String iePath = propertyReader.getProperty("localIEPath");
			logger.info("IEdriver location is " + iePath);
			System.setProperty("webdriver.IE.driver", iePath);
			logger.info("IEdriver location is " + iePath);
			driver = new InternetExplorerDriver();
		} else {
			String safariPath = propertyReader.getProperty("safariPath");
			logger.info("Safaridriver location is " + safariPath);
			System.setProperty("webdriver.safari.driver", safariPath);
			logger.info("Safaridriver location is " + safariPath);
			driver = new SafariDriver();

		}

		logger.info("Esecution Started");

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		logger.info("Maximizing the window ");
		driver.manage().window().maximize();
		logger.info("loading firefox browser");
		ShoppingKartBasePage.driver=driver;
//		ShoppingKartListener.driver=driver;
	} 

	@BeforeSuite
	public void startup() {
		logger.info("Launching shoppingkart url ");
		driver.get("http://mishoppingkart.com/ShoppingCart/");
	}

	@AfterSuite
	public void afterSuite() {
		logger.info("In After Suite");
		driver.quit();
		logger.info("closing browser session");
	}
}
