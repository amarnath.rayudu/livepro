package com.shoppingkart.test.home;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.shoppingkart.pages.home.ShoppingKartHomePage;
import com.shoppingkart.test.base.ShoppingKartBaseTest;

public class ShoppingKartHomeTest extends ShoppingKartBaseTest {
	public static final Logger logger = LogManager.getLogger(ShoppingKartHomeTest.class);
	ShoppingKartHomePage homePage;

	@Test(groups = "sanity")
	public void shoppinkartLoadTest() {
		homePage = new ShoppingKartHomePage();
		assertTrue(homePage.isHomePageLoaded(), "Homepage is not loaded ");
		logger.info(" verified the homepage");
	}

	@Test(dependsOnMethods = "shoppinkartLoadTest", groups = "sanity")
	public void isTitlePresentTest() {
		homePage = new ShoppingKartHomePage();
		String expectedTitle = "Shopping Cart Application";
		assertTrue(homePage.getPageTitle().equals(expectedTitle), "Title is not same");
		logger.info(" Verified the Title");
	}

}
