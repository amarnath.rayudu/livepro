package com.shoppingkart.test.api.product;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.annotations.Test;

public class ProductAPITest1 {
	// Step 1: We need create ID or http client object
	HttpClient httpClient = HttpClientBuilder.create().build();

	@Test
	public void createProductTest() throws ClientProtocolException, IOException {
		// Step 2: We need to post Url
		HttpPost httpPost = new HttpPost("http://mishoppingkart.com/ShoppingCart/api/addProduct");
		StringEntity productPayload = new StringEntity(
				"{\"name\": \"pranitha-HTTPProduct2\",\"category\": \"Instrument\",\"description\": \"Piano Indian Instruments\",\"price\": 23,\"condition\": \"new\",\"status\": \"Active\",\"units\": 20,\"manufacturer\": \"Piano India INC\",\"image\": null,\"imageUrl\": null}");
		// Step 3: add the format
		productPayload.setContentType("application/json");
		httpPost.setEntity(productPayload);
		// Step 4: look for the response
		HttpResponse response = httpClient.execute(httpPost);
		System.out.println(response.getStatusLine().getStatusCode());
		// out put you will see 201 status code
		String line = null;
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}

	@Test
	public void updateProductTest() throws ClientProtocolException, IOException {
		// Step 2: We need to post Url
		HttpPut httpPut = new HttpPut("http://mishoppingkart.com/ShoppingCart/api/viewProduct/");
		StringEntity productPayload = new StringEntity(
				"{\"id\":144, \"name\": \"pranitha-update\",\"category\": \"Instrument\",\"description\": \"apple Indian Instrumentsss\",\"price\": 23,\"condition\": \"new\",\"status\": \"Active\",\"units\": 20,\"manufacturer\": \"Piano India INC\",\"image\": null,\"imageUrl\": null}");
		// Step 3: add the format
		productPayload.setContentType("application/json");
		httpPut.setEntity(productPayload);
		// Step 4: look for the response
		HttpResponse response = httpClient.execute(httpPut);
		System.out.println(response.getStatusLine().getStatusCode());
		// out put you will see 201 status code
		String line = null;
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}

	@Test
	public void getProductTest() throws ClientProtocolException, IOException {
		HttpGet httpGet = new HttpGet("http://mishoppingkart.com/ShoppingCart/api/viewProduct/144");
		httpGet.addHeader("accept", "application/json");
		HttpResponse response = httpClient.execute(httpGet);
	}

	@Test
	public void deleteProductTest() throws ClientProtocolException, IOException {
		HttpDelete httpDelete = new HttpDelete("http://mishoppingkart.com/ShoppingCart/api/deleteProduct/144");
		httpDelete.addHeader("accept", "application/json");
		HttpResponse response = httpClient.execute(httpDelete);
		System.out.println(response.getStatusLine().getStatusCode());
		// out put you will see 201 status code
		String line = null;
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
	}

}
