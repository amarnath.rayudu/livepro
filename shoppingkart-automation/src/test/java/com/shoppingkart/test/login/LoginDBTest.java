package com.shoppingkart.test.login;

import static org.testng.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.shoppingkart.pages.home.ShoppingKartHomePage;
import com.shoppingkart.pages.login.ShoppingKartLoginPage;
import com.shoppingkart.pages.product.ShoppingKartProductPage;
import com.shoppingkart.test.base.ShoppingKartBaseTest;
import com.shoppingkart.test.product.ShoppingKartProductTest;
import com.shoppingkart.utils.DBReader;

public class LoginDBTest extends ShoppingKartBaseTest {
	public static final Logger logger = LogManager.getLogger(ShoppingKartProductTest.class);
	ShoppingKartHomePage homePage;
	ShoppingKartLoginPage loginPage;
	ShoppingKartProductPage productPage;

	@Test
	public void isHomeLoginPresentTest() {
		homePage = new ShoppingKartHomePage();
		assertTrue(homePage.isHomeLoginPresent(), "Login tab in ShoppingKart page is not loaded ");
	}

	@Test(dependsOnMethods = "isHomeLoginPresentTest")
	public void navigateToLoginPageTest() {
		homePage = new ShoppingKartHomePage();
		loginPage = homePage.clickOnLogin();
		assertTrue(loginPage.isLoginHeaderPresent(), "Login Header is not present");
	}

	@Test(dependsOnMethods = "navigateToLoginPageTest")
	public void loginDBcredentials() throws SQLException {
		ResultSet rs = DBReader.runQuery("select * from Users;");
		while (rs.next()) {
			logger.info("usernmae from database:" + rs.getString("username"));
			logger.info("password from database:" + rs.getString("password"));
			loginPage.login(rs.getString("username"), rs.getString("password"));
			break;
		}
		assertTrue(loginPage.isValidMessagePresent(), "validmessage after login not working");

	}

	@Test(dependsOnMethods = "loginDBcredentials")
	public void isProductPageLoadedTest() {
		productPage = homePage.clickOnProducts();
		System.out.println("product page clicked");
		assertTrue(productPage.isProductPageLoaded(), "Product page is not loaded");
	}

	@Test(dependsOnMethods = "isProductPageLoadedTest")
	public void highestPriceProductTest() throws SQLException {
		ResultSet rs = DBReader.runQuery("select * from product order by price desc;");
		while (rs.next()) {
			logger.info("the product in search " + rs.getString("name"));
			productPage.searchForProduct(rs.getString("name"));
			break;
		}
		assertTrue(productPage.isHighPriceProductPresent(), "highest price product not available");

	}

	@Test(dependsOnMethods = "highestPriceProductTest")
	public void categoryProductSearchTest() throws SQLException {
		ResultSet rs = DBReader.runQuery("select * from product where category='electronics';");
		while (rs.next()) {
			logger.info("the product in search " + rs.getString("category"));
			productPage.searchForProduct(rs.getString("category"));
			break;
		}

	}

	@Test(dependsOnMethods = "categoryProductSearchTest")
	public void lessPriceProductSearchTest() throws SQLException {
		ResultSet rs = DBReader.runQuery("select * from product where price<'30';");
		while (rs.next()) {
			logger.info("the product in search " + rs.getString("price"));
			productPage.searchForProduct(rs.getString("price"));
		}
	}
	

}
