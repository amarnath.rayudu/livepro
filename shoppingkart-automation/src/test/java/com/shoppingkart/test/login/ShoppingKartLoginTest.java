package com.shoppingkart.test.login;

import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import com.shoppingkart.pages.home.ShoppingKartHomePage;
import com.shoppingkart.pages.login.ShoppingKartLoginPage;
import com.shoppingkart.pages.product.ShoppingKartProductPage;
import com.shoppingkart.test.base.ShoppingKartBaseTest;

public class ShoppingKartLoginTest extends ShoppingKartBaseTest {
	public static final Logger logger = LogManager.getLogger(ShoppingKartLoginTest.class);
	ShoppingKartHomePage homePage;
	ShoppingKartLoginPage loginPage;
	ShoppingKartProductPage productPage;

	@Test(groups = "sanity")
	public void isHomeLoginPresentTest() {
		homePage = new ShoppingKartHomePage();
		assertTrue(homePage.isHomeLoginPresent(), "Login tab in ShoppingKart page is not loaded ");
	}

	@Test(dependsOnMethods = "isHomeLoginPresentTest", groups = "sanity")
	public void navigateToLoginPageTest() {
		homePage = new ShoppingKartHomePage();
		loginPage = homePage.clickOnLogin();
		assertTrue(loginPage.isLoginHeaderPresent(), "Login Header is not present");
	}

	@Test(dependsOnMethods = "navigateToLoginPageTest")
	public void credentialFieldTest() {
		loginPage = new ShoppingKartLoginPage();
		assertTrue(loginPage.isUserNameFieldPresent(), "Username Field is not present");
		logger.info(" verified the username field");
		assertTrue(loginPage.isPasswordFieldPresent(), "Password Field is not present");
	}

	@Test(dependsOnMethods = "credentialFieldTest")
	public void inValidLoginTest() {
		loginPage = loginPage.invalidLogin("aaaa", "aaaa");
		assertTrue(loginPage.isInvalidCredentialsErrorPresent(), "there is no invalid message");
	}

}
