package com.shoppingkart.pages.product;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.shoppingkart.pages.base.ShoppingKartBasePage;

public class ShoppingKartProductPage extends ShoppingKartBasePage {
	public static final Logger logger = LogManager.getLogger(ShoppingKartProductPage.class);
	ShoppingKartProductPage productPage;

	@FindBy(xpath = "//th[text()='thumbNail']")
	WebElement thumbNail;
	@FindBy(xpath = "//th[text()='Product Name']")
	WebElement productName;
	@FindBy(xpath = "//th[text()='Category']")
	WebElement category;
	@FindBy(xpath = "//th[text()='Condition']")
	WebElement condition;
	@FindBy(xpath = "//th[text()='Price']")
	WebElement price;
	@FindBy(xpath = "//a[contains(text(),'Welcom')]")
	WebElement welcomeText;

	@FindBy(xpath = "//a[contains(@href,'login')]")
	WebElement loginTab;

	@FindBy(xpath = "//input[@type='search']")
	private WebElement searchBox;
	@FindBy(xpath = "//table[@role='grid']//td[text()='ploptop']")
	WebElement highPriceProduct;

	@FindBy(xpath = "//td[@class='sorting_1']")
	WebElement resultsRow;

	public boolean isProductPageLoaded() {
		logger.info(" verifying the page loading");
		return isElementPresent(price);

	}

	public boolean areResultsLoaded() {
		logger.info(" verifying the results ");
		return isElementPresent(resultsRow);

	}

	public boolean thumbnailPresent() {
		logger.info(" verifying the thumbNail");
		return isElementPresent(thumbNail);

	}

	public boolean productNamePresent() {
		logger.info(" verifying the productName");
		return isElementPresent(productName);

	}

	public boolean categoryPresent() {
		logger.info(" verifying the category");
		return isElementPresent(category);

	}

	public boolean conditionPresent() {
		logger.info(" verifying the condition");
		return isElementPresent(condition);

	}

	public boolean pricePresent() {
		logger.info(" verifying the price");
		return isElementPresent(price);

	}

	public boolean isWelcomePagePresent() {
		logger.info(" Verifying the Welcome text after logging in");
		return isElementPresent(welcomeText);
	}

	public ShoppingKartProductPage searchForProduct(String productName) {

		searchBox.sendKeys(productName);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		searchBox.clear();

		return new ShoppingKartProductPage();
	}

	public boolean isLoginTabPresent() {
		logger.info(" verifying the loginTab after logging in");
		return isElementPresent(loginTab);
	}

	public boolean isHighPriceProductPresent() {
		logger.info(" verifying the highest price product ");
		return isElementPresent(highPriceProduct);
	}

}
