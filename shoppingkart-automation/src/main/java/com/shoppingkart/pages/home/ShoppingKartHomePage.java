package com.shoppingkart.pages.home;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.shoppingkart.pages.base.ShoppingKartBasePage;
import com.shoppingkart.pages.login.ShoppingKartLoginPage;
import com.shoppingkart.pages.product.ShoppingKartProductPage;

public class ShoppingKartHomePage extends ShoppingKartBasePage {
	public static final Logger logger = LogManager.getLogger(ShoppingKartHomePage.class);
	@FindBy(xpath = "/html/head/title")
	public WebElement title;
	@FindBy(xpath = "//a[contains(text(),'Shopping Cart')]")
	WebElement logo;
	@FindBy(xpath = "//a[text()='Products']")
	public WebElement products;
	@FindBy(xpath = "//a[contains(@href,'login')]")
	WebElement loginTab;

	public boolean isHomePageLoaded() {
		logger.info(" Verifying the homepage");
		return isElementPresent(logo);
	}

	public String getPageTitle() {
		logger.info(" Verifying the title");
		return driver.getTitle();

	}

	public ShoppingKartProductPage clickOnProducts() {
		logger.info("Clicking on product link");
		products.click();
		return new ShoppingKartProductPage();

	}

	public boolean isHomeLoginPresent() {
		logger.info(" Veryfying the login tab before logging in");
		return isElementPresent(loginTab);
	}

	public ShoppingKartLoginPage clickOnLogin() {
		logger.info(" Clicking on login Tab");
		loginTab.click();
		return new ShoppingKartLoginPage();
	}

}
