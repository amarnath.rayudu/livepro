package com.shoppingkart.pages.login;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.shoppingkart.pages.base.ShoppingKartBasePage;

public class ShoppingKartLoginPage extends ShoppingKartBasePage {
	public static final Logger logger = LogManager.getLogger(ShoppingKartLoginPage.class);
	ShoppingKartLoginPage loginPage;

	@FindBy(xpath = "//h2[contains(text(),'Login')]")
	WebElement loginHeader;
	@FindBy(id = "username")
	WebElement userField;
	@FindBy(id = "password")
	WebElement passwordField;
	@FindBy(xpath = "//input[@type='submit']")
	WebElement submitButton;
	@FindBy(className = "error")
	WebElement invalidMessage;
	@FindBy(xpath="//a[contains(text(),'admin')]")
	WebElement validMessage;

	public boolean isLoginHeaderPresent() {
		logger.info(" verifying the login header");
		return isElementPresent(loginHeader);
	}

	public boolean isUserNameFieldPresent() {
		logger.info(" verifying the username field");
		return isElementPresent(userField);
	}

	public boolean isInvalidCredentialsErrorPresent() {
		logger.info(" verifying the error message field");
		return isElementPresent(invalidMessage);
	}

	public boolean isPasswordFieldPresent() {
		logger.info(" verifying the pasword field");
		return isElementPresent(passwordField);
	}

	public ShoppingKartLoginPage invalidLogin(String invalidUsername, String invalidPassword) {
		logger.info(" entering invalid username and password");
		userField.sendKeys(invalidUsername);
		passwordField.sendKeys(invalidPassword);
		submitButton.click();
		return new ShoppingKartLoginPage();
	}

	public ShoppingKartLoginPage login(String username, String password) {
		logger.info(" entering invalid username and password");
		userField.sendKeys(username);
		passwordField.sendKeys(password);
		submitButton.click();
		return new ShoppingKartLoginPage();
	}

	public ShoppingKartLoginPage login() {
		logger.info(" entering default username and password");
		userField.sendKeys("amar");
		passwordField.sendKeys("amar");
		submitButton.click();
		return new ShoppingKartLoginPage();
	}
	public boolean isValidMessagePresent() {
		logger.info(" verifying the valid message after login");
		return isElementPresent(validMessage);
	}
	
}
