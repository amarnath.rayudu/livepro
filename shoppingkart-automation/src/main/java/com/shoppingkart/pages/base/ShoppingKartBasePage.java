package com.shoppingkart.pages.base;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ShoppingKartBasePage {
	public static WebDriver driver;
	public static final Logger logger = LogManager.getLogger(ShoppingKartBasePage.class);

	public ShoppingKartBasePage() {
		logger.info("Initializing pagefactory elements");
		PageFactory.initElements(driver, this);
		logger.info("setting the timeout ");

	}

	public boolean isElementPresent(WebElement element) {
		boolean elementPresent = false;

		try {
			elementPresent = element.isDisplayed();
		} catch (NoSuchElementException e) {
			logger.info(" Locator not found, please verify the locator and see whether page is properly loaded ");
		}
		return elementPresent;
	}

	public void waitForElement(WebElement element) {

		for (int i = 0; i < 60; i++) {

			if (isElementPresent(element))
				break;
			else
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}

	}
}
