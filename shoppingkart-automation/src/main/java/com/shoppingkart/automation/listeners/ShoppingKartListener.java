//package com.shoppingkart.automation.listeners;
//
//import java.io.File;
//import java.io.IOException;
//import java.lang.reflect.Method;
//
//import org.apache.commons.io.FileUtils;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.WebDriver;
//import org.testng.ITestContext;
//import org.testng.ITestNGMethod;
//import org.testng.ITestResult;
//import org.testng.TestListenerAdapter;
//
//public class ShoppingKartListener extends TestListenerAdapter {
//	public static WebDriver driver;
//
//	/** The Constant logger. */
//	private static final Logger logger = LogManager.getLogger(ShoppingKartListener.class.getName());
//
//	/**
//	 * on test start
//	 * 
//	 * @param {ITestResult} 
//	 *            tr
//	 */
//	@Override
//	public void onTestStart(ITestResult tr) {
//		ITestNGMethod testNGMethod = tr.getMethod();
//		Method method = testNGMethod.getConstructorOrMethod().getMethod();
//		logger.info("Staretd the test " + method.getName());
//
//	}
//
//	/**
//	 * on configureation failure
//	 * 
//	 * @param {ITestResult}
//	 *            tr
//	 */
//	@Override
//	public void onConfigurationFailure(ITestResult tr) {
//		logger.info(" Test failed due to configuration issues ");
//
//	}
//
//	/**
//	 * on test success
//	 * 
//	 * @param {ITestResult}
//	 *            tr
//	 */
//	@Override
//	public void onTestSuccess(ITestResult tr) {
//		ITestNGMethod testNGMethod = tr.getMethod();
//		Method method = testNGMethod.getConstructorOrMethod().getMethod();
//		logger.info(method.getName() + " is successful");
//
//		takeScreenShot(method.getName() + ".png");
//
//	}
//
//	/**
//	 * on test failure
//	 * 
//	 * @param {ITestResult}
//	 *            tr
//	 */
//	@Override
//	public void onTestFailure(ITestResult tr) {
//		ITestNGMethod testNGMethod = tr.getMethod();
//		Method method = testNGMethod.getConstructorOrMethod().getMethod();
//		logger.info(method.getName() + " is failed");
//		takeScreenShot(method.getName() + ".png");
//
//	}
//
//	/**
//	 * on test skipped
//	 * 
//	 * @param {ITestResult}
//	 *            tr
//	 */
//	@Override
//	public void onTestSkipped(ITestResult tr) {
//		ITestNGMethod testNGMethod = tr.getMethod();
//		Method method = testNGMethod.getConstructorOrMethod().getMethod();
//		logger.info(method.getName() + " is skipped");
//
//	}
//
//	/**
//	 * on finish
//	 * 
//	 * @param {ITestContext}
//	 *            test Context
//	 */
//	@Override
//	public void onFinish(ITestContext testContext) {
//		logger.info(" Test is compelted");
//
//	}
//
//	// reusable method to take screenshot
//
//	public void takeScreenShot(String screenshotName) {
//		try {
//			// Take screenshot create Screen file
//			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//
//			// Then store it somewhere, in this case it is project base folder
//
//			FileUtils.copyFile(scrFile, new File("screenshots/" + screenshotName));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//}
