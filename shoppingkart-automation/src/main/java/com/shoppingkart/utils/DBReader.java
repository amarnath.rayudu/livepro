package com.shoppingkart.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DBReader {
	private static Connection connection;
	private static Statement stmt;
	private static ResultSet rs;
	public static final Logger logger = LogManager.getLogger(DBReader.class);

	public static ResultSet runQuery(String query) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection("jdbc:mysql://162.253.125.192:3306/mishoppi_shoppingcart",
					"mishoppi_test", "testuser123#@!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			stmt = connection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;

	}

	public static String getFirstProduct() {
		logger.info(" Executing query to retrieve product ");

		ResultSet rs = runQuery("select * from product");
		String product = null;
		try {
			while (rs.next()) {
				product = rs.getString("name");
				break;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return product;

	}

	public static String getFirstUserId() {
		logger.info(" Executing query to retrieve user id");
		ResultSet rs = runQuery("select * from Users");
		String username = null;
		try {
			while (rs.next()) {
				username = rs.getString("username");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return username;

	}

	public static String getFirstUserPwd() {
		logger.info(" Executing query to retrieve user password");

		ResultSet rs = runQuery("select * from Users");
		String password = null;
		try {
			while (rs.next()) {
				password = rs.getString("password");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return password;

	}
}
