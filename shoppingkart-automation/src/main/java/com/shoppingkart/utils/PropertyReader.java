package com.shoppingkart.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropertyReader {
	public static final Logger logger = LogManager.getLogger(PropertyReader.class.getName());
	public String fileName;

	public PropertyReader(String fileName) {
		super();
		this.fileName = fileName;
	}

	public String getProperty(String key) {
		Properties properties = new Properties();
		InputStream inputstrem = null;
		try {
			inputstrem = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			properties.load(inputstrem);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("Reading the property file for= " + key);
		logger.info("value for the key in the property file is= " + properties.getProperty(key));
		return properties.getProperty(key);
	}

}
